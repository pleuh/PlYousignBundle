<?php


namespace Pl\YousignBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Pl\YousignBundle\Exception\CantDownloadSignedFileException;
use Pl\YousignBundle\Interfaces\CosignableInterface;
use Pl\YousignBundle\Interfaces\CosignerInterface;
use Pl\YousignBundle\Exception\PlYousignException;
use YousignAPI\YsApi;
use \YousignAPI_YsApi;

/**
 * Class YousignManager
 * @package Pl\YousignBundle\Manager
 * @property YsApi $yousignClient
 * @property EntityManagerInterface $em
 */
class YousignManager
{

	const STATE_SIGNED = "COSIGNATURE_FILE_SIGNED";
	const STATE_PROCESSING = "COSIGNATURE_FILE_PROCESSING";
	const STATE_PENDING = "COSIGNATURE_FILE_REQUEST_PENDING";

	protected $yousignClient, $em;

	/**
	 * YousignManager constructor.
	 * @param EntityManagerInterface $em
	 * @param $environment
	 * @param $login
	 * @param $password
	 * @param $api_key
	 */
	public function __construct(EntityManagerInterface $em, $environment
	, $login, $password, $api_key){
		$this->em = $em;

		$this->yousignClient = new \YousignAPI\YsApi();
		$this->yousignClient->setEnvironment($environment);
		$this->yousignClient->setApiKey($api_key);
		$this->yousignClient->setLogin($login);
		$this->yousignClient->setPassword($this->yousignClient->encryptPassword($password));
	}


	public function getCosignIframe(CosignableInterface $cosignable, CosignerInterface $cosigner, $contratContent){

		$state = $this->getCosign($cosignable)->getStatus();
		if(!$state){
			$this->initCosign($cosignable, $cosigner, $contratContent);
		}

		else{
			if($cosignable->getYousignId() && !in_array($state, [self::STATE_PROCESSING, self::STATE_SIGNED])){
				$this->yousignClient->deleteCosignDemand($cosignable->getYousignId());
				$cosignable->setYousignId(null);
				$this->em->persist($cosignable);
				$this->em->flush();
				$this->initCosign($cosignable, $cosigner, $contratContent);
			}
			else{
				throw new PlYousignException("Mauvaise données de cosign");
			}
		}



		$cosign = $this->getCosign($cosignable);
		$url = $this->yousignClient->getIframeUrl($cosign->getTokenFromCosigner($cosigner));

		return $url;
	}



	private function initCosign(CosignableInterface $cosignable, CosignerInterface $cosigner, $contratContent){

		// Création de la liste des fichiers à signer
		$listFiles = [[
				'name' => basename($cosignable->getContratFilePath()),
				'content' => base64_encode($contratContent),
				'idFile' => $cosignable->getContratFilePath()
			]];

		$listPerson = [[
				'firstName' => $cosigner->getPrenom(),
				'lastName' => $cosigner->getNom(),
				'mail' =>  $cosigner->getEmail(),
				'phone' => $this->sanitizePhone($cosigner->getTelephone()),
				'proofLevel' => 'LOW'
			]];



		$visibleOptions =
			[
				$listFiles[0]['idFile'] =>
					array_map(function($data) use($cosigner){
						return [
							'visibleSignaturePage' => (string)$data["page"],
							'isVisibleSignature' => true,
							'visibleRectangleSignature' => $data["position"],
							'mail' => $cosigner->getEmail()
						];
					}, $cosignable->getContratFileSignaturesData())
				,
			];



		// Message vide car on est en mode Iframe
		$message = '';

		// Autres options
		$options =
			[
				'mode' => 'IFRAME',
				'archive' => false
			];


		// Appel du client et récupération du résultat
		$result = $this->yousignClient->initCoSign($listFiles, $listPerson, $visibleOptions, $message, $options);

		if($result === false) {
			throw new PlYousignException("Impossible d'initier la cosignature : " . $this->yousignClient->getErrors()[0]);
		}


		// Récupération des "tokens" et création des liens d'accès aux documents à signer
		// S'il n'y a qu'un seul token, on met la variable sous forme de tableau
		if(isset($result['tokens']['token'])){
			$result['tokens'] = [$result['tokens']];
		}
		$cosignable->setYousignId($result["idDemand"]);
		$this->em->persist($cosignable);
		$this->em->flush();
	}


	/**
	 * @param $phoneStr
	 * @return string
	 */
	private function sanitizePhone($phoneStr){
		$phoneStr = preg_replace("#[ )(+]#", "", $phoneStr);
		if(strlen($phoneStr) == 12 and preg_match('#^(\d\d)0#', $phoneStr, $m)){
			$phoneStr = $m[1] . substr($phoneStr, 3);
		}
		if(strlen($phoneStr) == 10 and preg_match('#^0#', $phoneStr)){
			$phoneStr = "33" . substr($phoneStr, 1);
		}

		$phoneStr = "+".$phoneStr;
		return $phoneStr;
	}

	/**
	 * @param CosignableInterface $cosignable
	 * @return Cosign
	 */
	public function getCosign(CosignableInterface $cosignable){
		$cosignArInfos = $this->yousignClient->getCosignInfoFromIdDemand($cosignable->getYousignId());
		return new Cosign($cosignArInfos);
	}

	/**
	 * @param CosignableInterface $cosignable
	 * @param $filePath
	 * @throws CantDownloadSignedFileException
	 */
	public function downloadSignedFile(CosignableInterface $cosignable){
		$fileId = $this->getCosign($cosignable)->getFileInfoId();

		$ret = $this->yousignClient->getCosignedFileFromIdDemand($cosignable->getYousignId(), $fileId);

		if(array_key_exists("file", $ret)){
			return base64_decode($ret['file']);
		}
		else{
			throw new CantDownloadSignedFileException("Cant download the signer version from yousign");
		}
	}


}




class Cosign{
	protected $fileInfos;
	protected $dateCreation;
	protected $description;
	protected $status;
	protected $cosignerInfos;
	protected $initiator;

	/**
	 * Cosign constructor.
	 * @param $dateCreation
	 * @param $description
	 * @param $status
	 * @param $cosignerInfos
	 * @param $initiator
	 * @param $fileInfo
	 */
	public function __construct($cosignArInfos){
		if($cosignArInfos && is_array($cosignArInfos)){
			$this->dateCreation = $cosignArInfos["dateCreation"];
			$this->description = $cosignArInfos["description"];
			$this->status = $cosignArInfos["status"];
			$this->cosignerInfos = $cosignArInfos["cosignerInfos"];
			$this->initiator = $cosignArInfos["initiator"];
			$this->fileInfos = $cosignArInfos["fileInfos"];
		}
	}

	/**
	 * @return mixed
	 */
	public function getStatus(){
		if(is_array($this->fileInfos) && array_key_exists("cosignersWithStatus", $this->fileInfos) && array_key_exists("status", $this->fileInfos["cosignersWithStatus"])){
			return $this->fileInfos["cosignersWithStatus"]["status"];
		}
		return null;
	}

	public function getFileInfoId(){
		return $this->fileInfos["idFile"];
	}



	public function getTokenFromCosigner(CosignerInterface $cosigner){
		if(strtolower($this->cosignerInfos["mail"]) == strtolower($cosigner->getEmail())){
			return $this->cosignerInfos["token"];
		}
		throw new \Exception("Le cosigner demandé n'est pas dans la liste des signataires");
	}



}

















