<?php

namespace Pl\YousignBundle\Interfaces;


interface CosignableInterface
{
	public function setYousignId($yousignId);
	public function getYousignId();
	public function getContratFilePath();

	public function getContratFileSignaturesData();

}