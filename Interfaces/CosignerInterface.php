<?php

namespace Pl\YousignBundle\Interfaces;


interface CosignerInterface
{
	public function getPrenom();
	public function getNom();
	public function getEmail();
	public function getTelephone();
}