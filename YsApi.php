<?php

// Inclusion des fichiers requis
require_once dirname(__FILE__).'/lib/nusoap/nusoap.php';

class YousignAPI_YsApi
{
    const API_NAMESPACE = 'http://www.yousign.com';
    
    const API_ENV_DEMO = 'demo';
    const API_ENV_PROD = 'prod';

    const API_URL_DEMO = 'https://apidemo.yousign.fr:8181/';
    const API_URL_PROD = 'https://api.yousign.fr:8181/';

    const IFRAME_URL_DEMO = 'https://demo.yousign.fr';
    const IFRAME_URL_PROD = 'https://yousign.fr';

    /**
     * URL d'accès au WSDL de l'API Yousign d'authentification
     * @var string
     */
	private $URL_WSDL_AUTH = "";
	
    /**
     * URL d'accès au WSDL de l'API Yousign de signature
     * @var string
     */
	private $URL_WSDL_SIGN = "";
        
    /**
     * URL d'accès au WSDL de l'API Yousign de Co-signature
     * @var string
     */
    private $URL_WSDL_COSIGN = "";
    
    /**
     * URL d'accès au WSDL de l'API Yousign contenant les services généraux
     * @var string
     */
    private $URL_WSDL_COMMON = "";
    
    /**
     * URL d'accès au WSDL de l'API Yousign d'inscription
     * @var string
     */
    private $URL_WSDL_REGISTRATION = "";
    
    /**
     * URL d'accès au WSDL de l'API Yousign d'audit
     * @var string
     */
    private $URL_WSDL_AUDIT = "";

    /**
     * URL d'accès au WSDL de l'API Yousign d'archivage
     * @var string
     */
    private $URL_WSDL_ARCHIVE = "";

	/**
     * Contient le login de connexion au web service de l'utilisateur courant
     * @var string
     */
	private $_login = "";
	
	/**
     * Contient le mot de passe de connexion au web service en sha1
     * @var string
     */
	private $_password = "";
	
    /**
     * La clé d'API
     * @var string
     */
    private $apikey = "";

    /**
     * Url d'accès à l'API
     * @var string
     */
    private $urlApi = "";

    /**
     * URL d'accès à l'Iframe
     * @var string
     */
    private $urlIframe = "";

    /**
     * Définit l'utilisation ou non du protocol SSL
     * @var boolean
     */
    private $enabledSSL = false;

    /**
     * Emplacement du keystore client (SSL doit être actif)
     * 
     * @var string
     */
    private $certClientLocation = ""; 

    /**
     * Emplacement de la chaine de certification (SSL doit être actif)
     * 
     * @var string
     */
    private $caChainClientLocation = "";

    /**
     * Emplacement de la clef privée client  (SSL doit être actif)
     * 
     * @var string
     */
    private $privateKeyClientLocation = "";

    /**
     * Mot de passe de la clef privée client  (SSL doit être actif)
     * 
     * @var string
     */
    private $privateKeyClientPassword = "";

	/**
     * Définit si l'utilisateur est bien authentifié ou non
     * @var boolean
     */
	private $isAuthenticated = false;

    /**
     * Contient les paramètres d'accès à l'api pki
     * @var array
     */
    private $parameters = null;

    /**
     * Gestion des erreurs
     * @var array
     */
    private $errors = array();

	/**
	 * Permet de créer une nouvelle instance de la classe ysApi
     *
     * Il est possible de passer en paramètre le chemin d'un fichier properties (.ini)
     * 
     * Si c'est le cas, il peut contenir les clés suivantes :
     *     - url_api    : L'url d'accès à l'API
     *     - login      : L'identifiant Yousign (adresse email)
     *     - password   : Mot de passe Yousign
     *     - api_key    : La clé d'API
	 * 
     * @param $pathParametersFile : Chemin du fichier de configuration
     * @return YousignAPI_YsApi
	 */
	function __construct($pathParametersFile = null)
	{
        if($pathParametersFile !== null && file_exists($pathParametersFile))
            $this->parseParametersFile($pathParametersFile);
    }

    /**
     * Parse le fichier de configuration
     * 
     * @param  string $pathParametersFile Chemin du fichier de configuration
     * @return YousignAPI_YsApi
     */
    private function parseParametersFile($pathParametersFile)
    {
        $this->parameters = parse_ini_file($pathParametersFile, true);
        
        if(isset($this->parameters['environment']))
            $this->setEnvironment($this->parameters['environment']);
        
        if(isset($this->parameters['login'])) 
            $this->setLogin($this->parameters['login']);
        
        if(isset($this->parameters['password'])) 
        {
            $password = $this->parameters['password'];
            if(empty($this->parameters['isEncryptedPassword']) 
            || $this->parameters['isEncryptedPassword'] === false
            || $this->parameters['isEncryptedPassword'] === 'false')
                $password = $this->encryptPassword($password);

            $this->setPassword($password);
        }
        
        if(isset($this->parameters['api_key']))
            $this->setApiKey($this->parameters['api_key']);

        if(isset($this->parameters['ssl_enabled'])) 
        {
            $this->setEnabledSSL($this->parameters['ssl_enabled']);
            
            $this->setCertClientLocation($this->parameters['cert_client_location']);
            $this->setCaChainClientLocation($this->parameters['ca_chain_client_location']);
            $this->setPrivateKeyClientLocation($this->parameters['private_key_client_location']);
            $this->setPrivateKeyClientPassword($this->parameters['private_key_client_password']);
        }

        return $this;
    }

    /**
     * Modifie l'environnement de l'API utilisé
     * 
     * @param string $environment (demo|prod)
     */
    public function setEnvironment($environment)
    {
        switch ($environment) {
            // Environnement de production
            case self::API_ENV_PROD:
                $this->setUrlIframe(self::IFRAME_URL_PROD);
                $this->setUrlApi(self::API_URL_PROD);
                return $this;

            // Par défaut, environnement de démo
            case self::API_URL_DEMO:
            default:
                $this->setUrlIframe(self::IFRAME_URL_DEMO);
                $this->setUrlApi(self::API_URL_DEMO);
                return $this;
        }
    }

    /**
     * Modifie l'url d'accès à l'API
     * 
     * @param string $urlApi
     * @return  YousignAPI_YsApi
     */
    public function setUrlApi($urlApi)
    {
        $this->urlApi = $urlApi;

        // On créé les adresses des wsdl
        $this->URL_WSDL_AUTH = $this->urlApi."/AuthenticationWS/AuthenticationWS?wsdl";
        $this->URL_WSDL_SIGN = $this->urlApi."/SignatureWS/SignatureWS?wsdl";
        $this->URL_WSDL_COSIGN = $this->urlApi."/CosignWS/CosignWS?wsdl";
        $this->URL_WSDL_REGISTRATION = $this->urlApi."/RegistrationWS/RegistrationWS?wsdl";
        $this->URL_WSDL_COMMON = $this->urlApi."/CommonWS/CommonWS?wsdl";
        $this->URL_WSDL_AUDIT = $this->urlApi."/AuditWS/AuditWS?wsdl";
        $this->URL_WSDL_ARCHIVE = $this->urlApi."/ArchiveWS/ArchiveWS?wsdl";
        return $this;
    }

    /**
     * Modifie l'URL d'accès à l'Iframe.
     *
     * @param string $urlIframe
     * @return YousignAPI_YsApi
     */
    public function setUrlIframe($urlIframe)
    {
        $this->urlIframe = $urlIframe;

        return $this;
    }

    /**
     * Modification de l'identifiant d'accès à l'API
     * 
     * @param string $login
     * @return YousignAPI_YsApi
     */
    public function setLogin($login)
    {
        $this->_login = $login;

        return $this;
    }

    /**
     * Modification du mot de passe d'accès à l'API
     * 
     * @param string $password mot de passe encodé
     */
    public function setPassword($password)
    {
        $this->_password = $password;

        return $this;
    }

    /**
     * Modification de la clé d'API Yousign
     * 
     * @param string $apikey clé d'API
     * @return YousignAPI_YsApi
     */
    public function setApiKey($apikey)
    {
        $this->apikey = $apikey;

        return $this;
    }

    /**
     * Active ou non l'utilisation de SSL
     * 
     * @param  boolean $enabled
     * @return YousignAPI_YsApi
     */
    public function setEnabledSSL($enabled)
    {
        $this->enabledSSL = $enabled;

        return $this;
    }

    /**
     * Modification de l'emplacement de la chaine de certification
     * 
     * @param string $ca_chain_client_location the ca chain client location
     * @return YousignAPI_YsApi
     */
    public function setCaChainClientLocation($ca_chain_client_location)
    {
        $this->caChainClientLocation = $ca_chain_client_location;

        return $this;
    }

    /**
     * Modification de l'emplacement de la clef privée client  (SSL doit être actif).
     *
     * @param string $privateKeyClientLocation the private key client location 
     *
     * @return YousignAPI_YsApi
     */
    public function setPrivateKeyClientLocation($privateKeyClientLocation)
    {
        $this->privateKeyClientLocation = $privateKeyClientLocation;

        return $this;
    }

    /**
     * Modification du mot de passe de la clef privée client  (SSL doit être actif).
     *
     * @param string $privateKeyClientPassword the private key client password 
     *
     * @return YousignAPI_YsApi
     */
    public function setPrivateKeyClientPassword($privateKeyClientPassword)
    {
        $this->privateKeyClientPassword = $privateKeyClientPassword;

        return $this;
    }

    /**
     * Modification de l'emplacement du keystore client (SSL doit être actif).
     *
     * @param string $certClientLocation the cert client location 
     *
     * @return YousignAPI_YsApi
     */
    public function setCertClientLocation($certClientLocation)
    {
        $this->certClientLocation = $certClientLocation;

        return $this;
    }

    /**
     * Cryptage du mot de passe
     * 
     * @param  string $password mot de passe en clair
     * @return string mot de passe crypté
     */
    public function encryptPassword($password)
    {
        return sha1(sha1($password).sha1($password));
    }

    /**
     * Retourne l'url d'accès à l'iframe de signature du document
     * 
     * @param  string $token
     * @return string
     */
    public function getIframeUrl($token = '')
    {
        return $this->urlIframe.'/public/ext/cosignature/'.$token;
    }

    /**
     * Retourne les erreurs retournées par l'API
     * 
     * @return array
     */
    public function getErrors()
    {
        if(!is_array($this->errors))
            $this->errors = array($this->errors);
        return $this->errors;
    }
    

    /**
     * Permet de mettre en place le client de la requête en fonction du WSDL
     * @param urlWsdl : url du WSDL à traiter
     * @return Client SOAP
     */
    private function setClientSoap($urlWsdl)
    {
        // Instanciation du client SOAP
		$client = new nusoap_client($urlWsdl, false , false, false, false, false, 0, 1000);

        $client->soap_defencoding = 'UTF-8';
        $client->decode_utf8 = false;			
			 
        // Mise en place des options CURL
        // Option curl
        $client->setUseCurl(true);

        // Mise en place du SSl si on l'active
        if($this->enabledSSL)
        {
    		// Mise en place des données d'authentification SSL
    		$certRequest = array(
                'cainfofile' => $this->caChainClientLocation, 
				'sslcertfile' => $this->certClientLocation, 
				'sslkeyfile' => $this->privateKeyClientLocation, 
				'passphrase' => $this->privateKeyClientPassword
            ); 
    		
            $client->setCredentials('', '', 'certificate', $certRequest);
            $client->setCurlOption(CURLOPT_SSLVERSION, 3);
            // @TODO : cette option sera à mettre à true. On utilisera un fichier contenant l'AC Yousign en tant que trustore
            $client->setCurlOption(CURLOPT_SSL_VERIFYPEER, false);
        }
						
		// @TODO : voir comment on lève une exception
		$err = $client->getError();
		if ($err) 
		{
			echo '<h2>Constructor error</h2><pre>' . $err . '</pre>';
			echo '<h2>Debug</h2><pre>' . htmlspecialchars($client->getDebug(), ENT_QUOTES) . '</pre>';
			exit();
		}		
		
		return $client;
    }

	/**
	 * Permet de générer les headers nécessaire à l'authentification de l'utilisateur final
	 * @return String : headers contenant le login et le mot de passe de l'utilisateur final
	 */
	private function createHeaders($withUser = true)
	{
        if($withUser === true)
        {
		  return  '<apikey>'.$this->apikey.'</apikey>' . 
                  '<username>'.$this->_login.'</username>' . 
                  '<password>'.$this->_password.'</password>';
        }
        else
        {
            return '<apikey>'.$this->apikey.'</apikey>';   
        }
	}

	
	/**
	 * fonction de connexion à l'API
	 * @return TRUE si l'utilisateur peut se connecter, FALSE sinon
	 */
	public function connect()
	{
		// Paramètre du service de connexion
        //@todo : supprimer ce paramètre ??
		$params = array(
    		'arg0' => '1'
		);

        $client = $this->setClientSoap($this->URL_WSDL_AUTH);
		$result = $client->call('connect', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());

		if ($client->fault) 
		{
            $this->errors[] = $client->faultstring;
			return false;
		} 
		else 
		{
			$err = $client->getError();
			if ($err) 
			{
                $this->errors = $err;
				return $err;
			} 
			else 
			{
				if($result == "true")
				{
					$this->isAuthenticated = true;
					return true;
				}
				else
				{
					return false;
				}
			}
		}
	}
        
    /**
     * Fonction permettant de lancer une demande de signature de document
     * @param lstFilesToSign : liste des fichiers à signer accompagnés de 
     *                          leurs paramètres.
     * lstFilesToSign est un tableau, dont chaque élement est constitué comme ceci :
     * lstFilesToSign[x]
     *              [files] = Conteneur d'un fichier à signer et de ses paramètres 
     *                  [file] = Fichier en base64            
     *                  [fileName] = Nom du fichier
     *                  [params] = Conteneur des otpions de signature
     *                      [timestamp] = A mettre à TRUE, active le timestamping
     *                      [reason] = raison de la signature
     *                      [location] = position géographique de la signature
     *                      [visibleSignaturePage] = Numéro de la page sur laquelle on affiche la signature visible
     *                      [visibleSignature] = True si une signature visible doit être affichée, False sinon
     *                      [visibleRectangleSignature] = Emplacement de la signature visible selon cette notation -> llx,lly,urx,ury                               
     *                      [userPassword] = mot de passe utilisateur d'ouverture du PDF
     *                      [ownerPassword] = mot de passe du propriétaire du PDF
     */
    public function sign($lstFilesTosign)
    {          
        $payload = "";
        
        $client = $this->setClientSoap($this->URL_WSDL_SIGN);

        // A cause d'un soucis dans la librairie Nusoap, nous sommes obligés de créer nous même le payload afin qu'il corresponde à notre WSDL.
        // Le pb : Avec un tableau de paramètres, Nusoap créer cette arborescence : <files><item>...</item><item>...</item></files>
        // Nous avons besoin de cette arborescence : <files>...</files><files>...</files>. Les lignes suivantes permettent de bien la construire.
        foreach ($lstFilesTosign as $file) 
        {
            $item = array('files' => $file);
            foreach($item as $k => $v)
            {
                $payload .= $client->serialize_val($v,$k,false,false,false,false,'encoded');                   
            }
        }
        
        $result = $client->call('sign', $payload, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());            
        
        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring;
            return false;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err;
                return false;
            } 
            else 
            {
                if($result['status'] == "STRONG_AUTH_REQUIRED")
                {                      
                    return $result['idDemand'];
                }
                else
                {
                    return false;
                }
            }
        }
    }

    /**
     * Fonction permettant de confirmer la signature via un OTP (envoyé par SMS ou mail)
     * @param idDemand : id de la demande à confirmer
     * @param otp : code envoyé par otp         
     */
    public function confirmSign($idDemand, $otp, $signatureImage = "")
    {
        
        // Paramètre du service de connexion
        $params = array (
            'idDemand' => $idDemand,
            'strAuthParam' => $otp,
            'signatureImage' => $signatureImage);

        $client = $this->setClientSoap($this->URL_WSDL_SIGN);
        $result = $client->call('confirmSign', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());

        if ($client->fault)
        {	
            $this->errors[] = $client->faultstring;		
            if($client->faultstring == "Error 52 : Otp is not valid")
            {
                return "WRONG_OTP";
            }
            else
            {   
                if(strstr($client->faultstring, 'The demand might has been already done'))
                {
                    return 'DEMAND_ALREADY_DONE';
                }
                else
                {
                    return false;
                }
            }
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err;
                return false;
            } 
            else 
            {
                if($result['status'] == "OK")
                {                                   
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }
    
    /**
     * Fonction permettant d'obtenir des informations sur une demande de signature spécifique
     * @param idDemand : id de la demande à traiter        
     */
    public function getInfoFromDemand($idDemand)
    {
        
        // Paramètre du service de connexion
        $params = array('idDemand' => $idDemand);

        $client = $this->setClientSoap($this->URL_WSDL_SIGN);
        $result = $client->call('getInfosFromDemand', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());                        

        if($client->fault)
        {
            $this->errors[] = $client->faultstring; 
            if(strstr($client->faultstring, 'is not associated with the user'))
            {
                return 'DEMAND_NOT_ALLOWED';
            }
            else
            {
                return false;                
            }
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            }
            else 
            { 
                return $result;
            }
        }
    }
    
    /**
     * Fonction permettant d'obtenir des informations sur une demande de cosignature spécifique
     * 
     * @param  integer $idDemand Id de la demande de cosignature
     * @return mixed Retourne :
     *             - DEMAND_NOT_ALLOWED si l'id de la demande passé est incorrect
     *             - false si une erreur est survenue
     *             - Tableau contenant les informations de la demande de cosignature
     *                 - dateCreation : Date de creation de la demande de signature
     *                 - description : Description de la demande de signature
     *                 - status : Status global de la demande de signature
     *                 - fileInfos : Tableau contenant la liste des informations des fichiers à signer/signés
     *                     * idFile : Id du fichier
     *                     * fileName : Nom du fichier
     *                     * cosignersWithStatus : Status de la signature pour chacun des signataires 
     *                         + id : Id du signataire
     *                         + status : Status de la signature
     *                 - cosignerInfos : Tableau contenant la liste des informations du/des signataires
     *                     * firstName : Prénom
     *                     * lastName : Nom
     *                     * mail : Email
     *                     * proofLevel : Niveau de preuve de la signature
     *                     * isCosignerCalling : True si le token est associé au signataire, false sinon
     *                     * id : Id du signataire
     *                     * phone : Numéro de téléphone du signataire
     *                 - initiator : Tableau contenant les informations de l'initiateur de la demande de signature
     *                     * name : Nom + Prénom
     *                     * email : Email
     */
    public function getInfoFromCosignDemand($idDemand)
    {
        
        // Paramètre du service de connexion
        $params = array('idDemand' => $idDemand);

        $client = $this->setClientSoap($this->URL_WSDL_COSIGN);
        $result = $client->call('getInfosFromCosignatureDemand', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());                        

        if($client->fault) 
        {
            $this->errors[] = $client->faultstring; 
            if(strstr($client->faultstring, 'is not associated with the user'))
            {
                return 'DEMAND_NOT_ALLOWED';
            }
            else
            {
                return false;                
            }
        } 
        else 
        {
            $err = $client->getError();
            $this->errors = $err; 
            if ($err) 
            {
                return false;
            }
            else 
            {
                return $result;
            }
        }
    }
    
    /**
     * Fonction permettant de récupérer un fichier signer
     * @param idDemand : id de la demande à confirmer
     * @param idFile : id du fichier signé à récupérer
     * @ TODO : récupérer tous les fichiers d'un coup (dans un zip)
     * @ TODO : cette fonction doit remplacer getSignedFilesFromDemand()
     */
    public function getSignedFile($idDemand, $idFile)
    {            
        // Paramètre du service de connexion
        $params = array(
            'idDemand' => $idDemand,
            'idFile' => $idFile
        );
        
        $client = $this->setClientSoap($this->URL_WSDL_SIGN);
        $result = $client->call('getSignedFilesFromDemand', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());            
        
        if ($client->fault) 
        {	
            $this->errors[] = $client->faultstring; 

            // La demande n'est pas associée à l'utilisateur
            if(strstr($client->faultstring, "is not associated with the user"))
            {   
                return "DEMAND_NOT_ALLOWED";
            }
            else
            {
                // La demande n'a pas été validée, il est impossible de récupérer les fichiers signés
                if(strstr($client->faultstring, "has not been signed. Impossible to get signed files"))
                {
                    return "DEMAND_NOT_CONFIRMED";
                }
                else
                {
                    // Une erreur inconnue est apparue
                    return false;
                }
            }
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                // Tableau contenant :
                //      - fileName : le nom du fichier
                //      - file : le fichier signé
                $res = isset($result['file']) ? 
                    $result : $result[0];
                return $res;
            }
        }
    }
    
    /**
     * Fonction permettant de récupérer un fichier signé
     *
     * @TODO : récupérer tous les fichiers d'un coup (dans un zip)
     * @TODO : cette fonction doit remplacer getSignedFilesFromDemand()
     * 
     * @param  integer $idDemand identifiant de la demande retourné à la création d'un "initCosign"
     * @param  integer $idFile   identifiant d'un fichier à signer
     * @return mixed Retourne :
     *             - DEMAND_NOT_ALLOWED si la demande n'est pas associée à l'utilisateur
     *             - DEMAND_NOT_CONFIRMED si la demande n'a pas été validée 
     *             - false si une erreur est survenue
     *             - un tableau contenant:
     *                 -> fileName : Le nom du fichier
     *                 -> file : Le fichier signé encodé en base64
     */
    public function getCosignedFile($idDemand, $idFile)
    {            
        // Paramètre du service de connexion
        $params = array(
            'idDemand' => $idDemand,
            'idFile' => $idFile);
        
        $client = $this->setClientSoap($this->URL_WSDL_COSIGN);
        $result = $client->call('getCosignedFilesFromDemand', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());            
        
        if ($client->fault) 
        {   
            $this->errors[] = $client->faultstring; 

            // La demande n'est pas associée à l'utilisateur
            if(strstr($client->faultstring, "is not associated with the user"))
            {      
                return "DEMAND_NOT_ALLOWED";
            }
            else
            {
                // La demande n'a pas été validée, il est impossible de récupérer les fichiers signés
                if(strstr($client->faultstring, "has not been signed. Impossible to get signed files"))
                {
                    return "DEMAND_NOT_CONFIRMED";
                }
                else
                {
                    // Une erreur inconnue est apparue
                    return false;
                }
            }
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                // Tableau contenant :
                //      - fileName : le nom du fichier
                //      - file : le fichier signé
                $res = isset($result['file']) ? 
                    $result : $result[0];
                return $res;
            }
        }
    }

    /**
     * Fonction permettant de confirmer la signature via un OTP (envoyé par SMS ou mail)
     * @param idDemand : id de la demande à confirmer
     * @param $pathFile : chemin d'accès au répertoire dans lequel on doit enregistrer les fichiers signés
     * @return Tableau contenant les références des fichiers signés :
     *          - Les clefs du tableau correspondent aux noms temporaires des fichiers enregistrés dans le $pathFile 
     *          - Chaque valeur des clefs du tableau indique le véritable nom du fichier
     */
    public function getSignedFilesFromDemand($idDemand, $pathFile)
    {
        
        // Paramètre du service de connexion
        $params = array('idDemand' => $idDemand);
        
        // Liste des fichiers signés récupérés
        $fileNames = array();
        
        $client = $this->setClientSoap($this->URL_WSDL_SIGN);
        $result = $client->call('getSignedFilesFromDemand', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());
                    
        if ($client->fault) 
        {	
            $this->errors[] = $client->faultstring; 

            // La demande n'est pas associée à l'utilisateur
            if(strstr($client->faultstring, "is not associated with the user"))
            {
                return "DEMAND_NOT_ALLOWED";
            }
            else
            {
                // La demande n'a pas été validée, il est impossible de récupérer les fichiers signés
                if(strstr($client->faultstring, "has not been signed. Impossible to get signed files"))
                {  
                    return "DEMAND_NOT_CONFIRMED";
                }
                else
                {
                    // Une erreur inconnue est apparue 
                    return false;
                }
            }
        } 
        else 
        {
                $err = $client->getError();
                if ($err) 
                {
                    $this->errors = $err; 
                    return false;
                } 
                else 
                {
                    // Si on a qu'un seul fichier, on le récupère directement    
                    if(isset($result['file']))
                    {
                        // On génére une valeur aléatoire pour le nom du fichier
                        $fileName= sha1(microtime().rand());
                        while(file_exists($pathFile.'/'.$fileName))
                        {
                            $fileName = sha1(microtime().rand());
                        }
                        file_put_contents($pathFile.'/'.$fileName, base64_decode($result['file']));
                        $fileNames[$fileName] = $result['fileName'];
                    }
                    else
                    {
                        // Si on a plusieurs fichiers, on les récupère tous
                        foreach($result as $signedFile)
                        {
                            // On génére une valeur aléatoire pour le nom du fichier
                            $fileName= sha1(microtime().rand());
                            while(file_exists($pathFile.'/'.$fileName))
                            {
                                $fileName = sha1(microtime().rand());
                            }
                            file_put_contents($pathFile.'/'.$fileName, base64_decode($signedFile['file']));
                            $fileNames[$fileName] = $signedFile['fileName'];
                        }                                                   
                    }

                    return $fileNames;
                }
        }
    }
    
    /**
     * Cette méthode est utilisée pour initialiser une demande de cosignature.
     * Vous passerez en paramètre une liste de fichiers à signer ainsi qu'une liste d'informations des cosignataires.
     * Ils recevront ensuite un email contenant une URL unique pour accéder à l'interface de signature du/des documents afin de le/les signer.
     *
     * example:
     * ----------
     *     $listFiles = array(
     *         array(
     *             'name' => 'Fichier 1',
     *             'content' => 'base64 du fichier',
     *             'idFile' => 'idFile1'
     *         ),
     *         array(
     *             'name' => 'Fichier 2',
     *             'content' => 'base64 du fichier',
     *             'idFile' => 'idFile2'
     *         ),
     *     );
     *
     *     $lstPersons = array
     *     (
     *         array(
     *             'firstName' => 'Prenom 1',
     *             'lastName' => 'Nom 1',
     *             'mail' => 'prenom.nom1@mail.com',
     *             'phone' => '+33123456789',
     *             'proofLevel' => 'LOW'
     *         ),
     *         array(
     *             'firstName' => 'Prenom 1',
     *             'lastName' => 'Nom 1',
     *             'mail' => 'prenom.nom2@mail.com',
     *             'phone' => '+33123456789',
     *             'proofLevel' => 'LOW'
     *         ),
     *     );
     *
     *     $visibleOptions = array
     *     (
     *         $listFiles[0]['idFile'] => array
     *         (
     *             array(
     *                 'visibleSignaturePage' => '1',
     *                 'isVisibleSignature' => true,
     *                 'visibleRectangleSignature' => 'llx,lly,urx,ury',
     *                 'mail' => 'prenom.nom1@mail.com'
     *             ),
     *             array(
     *                 'visibleSignaturePage' => '1',
     *                 'isVisibleSignature' => true,
     *                 'visibleRectangleSignature' => 'llx,lly,urx,ury',
     *                 'mail' => 'prenom.nom2@mail.com'
     *             ),
     *         ),
     *         $listFiles[1]['idFile'] => array
     *         (
     *             array(
     *                 'visibleSignaturePage' => '3',
     *                 'isVisibleSignature' => true,
     *                 'visibleRectangleSignature' => 'llx,lly,urx,ury',
     *                 'mail' => 'prenom.nom1@mail.com'
     *             ),
     *             array(
     *                 'visibleSignaturePage' => '3',
     *                 'isVisibleSignature' => true,
     *                 'visibleRectangleSignature' => 'llx,lly,urx,ury',
     *                 'mail' => 'prenom.nom2@mail.com'
     *             ),
     *         )
     *     );
     *
     *     $message = 'Un message';
     *
     *     $options = array(
     *         'initMailSubject' => 'Sujet de l\'email',
     *         'initMail' => 'Contenu de l\'email'
     *         [...]
     *     );
     * 
     * @param  array $lstFiles          : Liste du/des fichiers à signer, chaque fichier doit définir:
     *                                      - name : Nom du fichier à signer
     *                                      - content : Contenu du fichier à signer encodé en base64
     *                                      - idFile : identifiant unique (entier ou chaine de caractère)
     * @param  array $lstPersons        : Liste des cosignataires, chaque cosignataire doit définir:
     *                                      - firstName : Le prénom du cosignataire
     *                                      - lastName : Le nom du cosignataire
     *                                      - mail : L'email du cosignataire (ou un id si c'est en mode Iframe)
     *                                      - phone : Le numéro de téléphone du cosignataire (indicatif requis, exemple: +33612326554)
     *                                      - proofLevel : Niveau de preuve
     *                                                     Disponible: LOW
     *                                                     Par défaut: Rien
     * @param  array $visibleOptions    : Liste d'informations requis pour le placement des signatures
     *                                      - visibleSignaturePage : Numéro de la page contenant les signatures
     *                                      - isVisibleSignature : Affiche ou non la signature sur le document
     *                                      - visibleRectangleSignature : Les coordonnées de l'image de signature (ignoré si "isVisibleSignature" est à false)
     *                                                                    Le format est "llx,lly,urx,ury" avec:
     *                                                                      * llx: left lower x coordinate
     *                                                                      * lly: left lower y coordinate
     *                                                                      * urx: upper right x coordinate
     *                                                                      * ury: upper right y coordinate
     *                                      - mail : Email du cosignataire associée à la signature
     * @param  string $message          : Message de l'email qui sera envoyé aux cosignataires (Non utilisé si initMailXXX définis)
     * @param  array  $options          : Tableau d'options facultatifs
     *                                      - initMailSubject : Sujet de l'email envoyé à tous les cosignataires à la création de la cosignature (Non utilisé en mode Iframe)
     *                                      - initMail : Corps de l'email envoyé à tous les cosignataires à la création de la cosignature.
     *                                                   Il doit être en HTML et contenir la balise {yousignUrl} qui sera remplacée par l'URL
     *                                                   d'accès à l'interface de signature du/des documents. (Non utilisé en mode Iframe)  
     *                                      - endMailSubject : Sujet de l'email envoyé lorsque tous les cosignataires ont signés le/les documents (Non utilisé en mode Iframe) 
     *                                      - endMail : Corps de l'email envoyé lorsque tous les cosignataires ont signés le/les documents
     *                                                  Il dit être en HTML et contenir la balise {yousignUrl} qui sera remplacée par l'URL
     *                                                  d'accès à l'interface listant le/les documents signés (Non utilisé en mode Iframe) 
     *                                      - language : Langue définie pour la cosignature. 
     *                                                   Disponibles: FR|EN|DE 
     *                                                   Par défaut: FR
     *                                      - mode :     Mode d'utilisation (Aucun par défaut) 
     *                                                     * IFRAME : Permet de signer directement dans l'application hébergeant l'iframe
     *                                                                Ceci retournera un token pour chaque signataire
     *                                                                L'URL devant appeler l'Iframe est:
     *                                                                    => (Démo) https://demo.yousign.fr/public/ext/cosignature/{token} 
     *                                                                    => (Prod) https://yousign.fr/public/ext/cosignature/{token}
     *                                      - archive : Booléen permettant d'activer l'archivage du/des documents signés automatiquement
     *                                                  L'archivage se fait lorsque tous les cosignataires ont signés
     * @return mixed : Id de la demande de cosignature créée et liste des id des fichiers à signer 
     *                 Si le mode "IFRAME" est définie, un token sera également retournée pour chaque cosignataire
     *                 Pour associer le bon token au bon cosignataire, un email et un numéro de téléphone sont associés à chaque token
     *                 (ou false si une erreur est survenue)
     * @category com.yousign.cosignejb
     * @link http://developer.yousign.fr/com/yousign/cosignejb/CosignWS.html#CosignWS() 
     */
    public function initCoSign($lstFiles, $lstPersons, $visibleOptions, $message, $options = array())
    {          
        $payload = "";

        $client = $this->setClientSoap($this->URL_WSDL_COSIGN);
        
        // A cause d'un soucis dans la librairie Nusoap, nous sommes obligés de créer nous même le payload afin qu'il corresponde à notre WSDL.
        // Le pb : Avec un tableau de paramètres, Nusoap créer cette arborescence : <files><item>...</item><item>...</item></files>
        // Nous avons besoin de cette arborescence : <files>...</files><files>...</files>. Les lignes suivantes permettent de bien la construire.
        
        // Liste des fichiers de la co-signature
        foreach ($lstFiles as $key => $file) 
        {
            $filePayload = "";
            $visibleOptionsPayload = "";
            
            // On récupère les options de signature visible                
            foreach ($visibleOptions[$file['idFile']] as $option) 
            {
                $item = array('visibleOptions' => $option);
                foreach($item as $k => $v)
                {
                    $visibleOptionsPayload .= $client->serialize_val($v,$k,false,false,false,false,'encoded');                   
                }
            }

            // On supprime l'id file qui est inutile
            unset($file['idFile']);

            $item = array('lstCosignedFile' => $file);
            foreach($item as $k => $v)
            {
                $filePayload .= $client->serialize_val($v,$k,false,false,false,false,'encoded');                   
            }

            // On insert les données de signature visible
            $payload .= substr_replace($filePayload, $visibleOptionsPayload, strpos($filePayload, '</lstCosignedFile>'), 0);
        }

        // Liste des co-signataires
        foreach ($lstPersons as $person) 
        {
            $item = array('lstCosignerInfos' => $person);
            foreach($item as $k => $v)
            {
                $payload .= $client->serialize_val($v,$k,false,false,false,false,'encoded');                   
            }
        }
        
        // Ajout du message
        $payload .= $client->serialize_val($message,'message',false,false,false,false,'encoded');
        
        // Envoi d'email
        if(isset($options['initMailSubject']) && isset($options['initMail'])) {
            $payload .= $client->serialize_val($options['initMailSubject'], 'initMailSubject', false, false, false, false, 'encoded');
            $payload .= $client->serialize_val($options['initMail'], 'initMail', false, false, false, false, 'encoded');
        }
        
        if(isset($options['endMailSubject']) && isset($options['endMail'])) {
            $payload .= $client->serialize_val($options['endMailSubject'], 'endMailSubject', false, false, false, false, 'encoded');
            $payload .= $client->serialize_val($options['endMail'], 'endMail', false, false, false, false, 'encoded');
        }

        if(isset($options['language'])) {
            $payload .= $client->serialize_val($options['language'], 'language', false, false, false, false, 'encoded');
        }

        if(isset($options['mode'])) {
            $payload .= $client->serialize_val($options['mode'], 'mode', false, false, false, false, 'encoded');
        }

        if(isset($options['archive'])) {
            $payload .= $client->serialize_val($options['archive'], 'archive', false, false, false, false, 'encoded');
        }

        $result = $client->call('initCosign', $payload, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());

        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring; 
            return false;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                if($result > 0)
                {
                    return $result;
                }
                else
                {
                    $this->errors[] = 'No result'; 
                    return false;
                }
            }
        }
    }

    /**
     * Fonction permettant de récupérer un fichier signer
     * 
     * @param  string $token    : Token unique associé à un signataire non enregistré
     * @param  integer $idFile  : Id du fichier signé à récupérer
     * @return mixed Retourne :
     *             - DEMAND_NOT_ALLOWED si la demande n'est pas associée à l'utilisateur
     *             - DEMAND_NOT_CONFIRMED si la demande n'a pas été validée 
     *             - false si une erreur est survenue
     *             - un tableau contenant:
     *                 -> fileName : Le nom du fichier
     *                 -> file : Le fichier signé encodé en base64
     *
     * @todo récupérer tous les fichiers d'un coup (dans un zip)
     */
    public function getCosignSignedFileFromToken($token, $idFile)
    {            
        // Paramètre du service de connexion
        $params = array(
                        'token' => $token,
                        'idFile' => $idFile
                        );

        $client = $this->setClientSoap($this->URL_WSDL_COSIGN);
        $result = $client->call('getCosignedFilesFromDemand', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders(false));                                    
        
        if ($client->fault) 
        {   
            $this->errors[] = $client->faultstring; 

            // La demande n'est pas associée à l'utilisateur
            if(strstr($client->faultstring, "is not associated with the user"))
            {                
                return "DEMAND_NOT_ALLOWED";
            }
            else
            {
                // La demande n'a pas été validée, il est impossible de récupérer les fichiers signés
                if(strstr($client->faultstring, "has not been signed. Impossible to get signed files"))
                {
                    return "DEMAND_NOT_CONFIRMED";
                }
                else
                {
                    // Une erreur inconnue est apparue
                    return false;
                }
            }
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                $res = isset($result['fileName']) ?
                    $result : $result[0];
                return $res;              
            }
        }
    }

    /**
     * Fonction permettant de récupérer un fichier signé d'une cosignature
     * 
     * @param  integer $idDemand    : Id de la demande de cosignature
     * @param  integer $idFile      : Id du fichier signé à récupérer
     * @return mixed Retourne :
     *             - DEMAND_NOT_ALLOWED si la demande n'est pas associée à l'utilisateur
     *             - DEMAND_NOT_CONFIRMED si la demande n'a pas été validée 
     *             - false si une erreur est survenue
     *             - un tableau contenant:
     *                 -> fileName : Le nom du fichier
     *                 -> file : Le fichier signé encodé en base64
     *
     * @todo récupérer tous les fichiers d'un coup (dans un zip)   
     */
    public function getCosignSignedFile($idDemand, $idFile)
    {            
        // Paramètre du service de connexion
        $params = array(
                        'idDemand' => $idDemand,
                        'idFile' => $idFile
                        );
        
        $client = $this->setClientSoap($this->URL_WSDL_COSIGN);
        $result = $client->call('getCosignedFilesFromDemand', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());                                    

        if ($client->fault) 
        { 
            $this->errors[] = $client->faultstring;   
            
            // La demande n'est pas associée à l'utilisateur
            if(strstr($client->faultstring, "is not associated with the user"))
            {     
                return "DEMAND_NOT_ALLOWED";
            }
            else
            {
                // La demande n'a pas été validée, il est impossible de récupérer les fichiers signés
                if(strstr($client->faultstring, "has not been signed. Impossible to get signed files"))
                {
                    return "DEMAND_NOT_CONFIRMED";
                }
                else
                {
                    // Une erreur inconnue est apparue 
                    return false;
                }
            }
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err;
                return false;
            } 
            else 
            {
                $res = isset($result['fileName']) ?
                    $result : $result[0];
                return $res;                   
            }
        }
    }

    /**
     * Cette fonction permet de récupérer les infos de l'utilisateur
     */
    function getUserInfos($mail = null)
    {
        if($mail == null)
        {
            // Paramètre du service de connexion
            $params = array();
        }
        else
        {
            // Paramètre du service de connexion
            $params = array(
                'mail' => $mail
                );
        }

        $client = $this->setClientSoap($this->URL_WSDL_REGISTRATION);
        $result = $client->call('getInfoUser', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());                        

        if($client->fault)
        {
            $this->errors[] = $client->faultstring; 
            return false;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            }
            else 
            {
                return $result;
            }
        }
    }

    /**
     * Fonction permettant de confirmer la co-signature via un OTP (envoyé par SMS ou mail)
     * 
     * @param  string $token          : Token unique associé à un signataire non enregistré
     * @param  string $otp            : Code d'authentification, envoyé par SMS ou email, permettant d'authentifier le signataire
     * @param  string $imageSignature : Image de la signature (encodé en base64)
     * @param  integer $idDemand      : Id de la demande de cosignature
     * @return mixed Retourne :
     *             - WRONG_OTP : Le code d'authentification passé est incorrect
     *             - DEMAND_ALREADY_DONE : La demande de signature a déjà été faites
     *             - false : Echec lors de la confirmation de la signature
     *             - true : Signature réalisée avec succés
     */
    public function confirmCosign($token, $otp, $imageSignature = null, $idDemand = null)
    {
        
        if($idDemand != null && !empty($idDemand))
        {
            // Paramètre du service de connexion
            $params = array(
                        'idDemand' => $idDemand,
                        'strAuthParam' => $otp
                        );
        }
        else
        {
            // Paramètre du service de connexion
            $params = array(
                        'token' => $token,
                        'strAuthParam' => $otp
                        );
        }

        if($imageSignature != null && !empty($imageSignature))
        {
            $params['signatureImage'] = $imageSignature;
        }
        
        $client = $this->setClientSoap($this->URL_WSDL_COSIGN);
        
        // Si on utilise l'idDemand, on s'authentifie
        if($idDemand != null && !empty($idDemand))
        {
            $result = $client->call('confirmCosign', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());
        }
        else
        {
            $result = $client->call('confirmCosign', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders(false));
        }
        
        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring;

            if($client->faultstring == "Error 52 : Otp is not valid")
            {
                return "WRONG_OTP";
            }
            else
            {   
                if(strstr($client->faultstring, 'The demand might has been already done'))
                {
                    return 'DEMAND_ALREADY_DONE';
                }
                else
                {
                    return false;
                }
            }
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                if($result === true || strtolower(trim($result)) == 'true')
                {                            
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }

    /**
     * Fonction permettant de signer une cosignature
     *
     * Remarque : Passer un token si le signataire n'est pas enregistré sur Yousign, sinon passé un idDemand.
     * 
     * @param  string $token        : Token unique associé à un signataire non enregistré
     *                                Il est utilisé pour identifier un signataire et récupérer les documents à signer.
     * @param  array $lstFiles      : Liste des fichiers à signer, chaque fichier doit contenir:
     *                                  - file : L'id du fichier à signer (retourné à la demande de cosignature)
     *                                  - [ reason : Raison de la signature ]
     *                                  - [ location : Localisation de la signature ]
     *                                  - [ userPassword : Mot de passe passé par le signataire si le pdf requiert un mot de passe ]
     *                                  - [ ownerPassword : Mot de passe passé par l'initiateur si le pdf requiert un mot de passe ]
     *                                  - [ isRefused : Définie si la signature a été refusée ]
     * @param  string $message      : Message spécifié par le signataire lors de la signature
     * @param  integer $idDemand    : Id de la demande de cosignature pour un signataire enregistré
     * @return boolean
     * 
     * @todo : Revoir la liste des paramètres
     */
    public function cosignFile($token, $lstFiles, $message, $idDemand = null)
    {          
        $payload = "";

        $client = $this->setClientSoap($this->URL_WSDL_COSIGN);

        // A cause d'un soucis dans la librairie Nusoap, nous sommes obligés de créer nous même le payload afin qu'il corresponde à notre WSDL.
        // Le pb : Avec un tableau de paramètres, Nusoap créer cette arborescence : <files><item>...</item><item>...</item></files>
        // Nous avons besoin de cette arborescence : <files>...</files><files>...</files>. Les lignes suivantes permettent de bien la construire.

        // Si on a un idDemand, on utilise l'idDemand à la place du token. De plus, on s'authentifiera
        if($idDemand != null && !empty($idDemand))
        {
            // Ajout de l'idDemand
            $payload .= $client->serialize_val($idDemand,'idDemand',false,false,false,false,'encoded');            
        }
        else
        {
            // Ajout du token
            $payload .= $client->serialize_val($token,'token',false,false,false,false,'encoded');
        }
        
        // Liste des fichiers de la co-signature
        foreach ($lstFiles as $file) 
        {                
            $item = array('lstCosignParameters' => $file);
            foreach($item as $k => $v)
            {
                $payload .= $client->serialize_val($v,$k,false,false,false,false,'encoded');                   
            }
        }

        // Ajout du message
        $payload .= $client->serialize_val($message,'message',false,false,false,false,'encoded');        
        
        // Si on utilise l'idDemand, on s'authentifie
        if($idDemand != null && !empty($idDemand))
        {
            $result = $client->call('cosign', $payload, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());
        }
        else
        {
            $result = $client->call('cosign', $payload, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders(false));
        }        

        if ($client->fault) 
        {           
            $this->errors[] = $client->faultstring; 
            return false;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                // @TODO : voir qu'est qu'on retourne
                if($result)
                {
                    return $result;
                }
                else
                {
                    return false;
                }
            }
        }
    }

    /**
     * Fonction permettant de récupérer les informations d'une demande de co-signature en fonction du token
     * 
     * @param  string $token : Token unique associé à une demande de cosignature
     * @return mixed Retourne :
     *             - INVALID_TOKEN si le token passé est incorrect
     *             - false si une erreur est survenue
     *             - Tableau contenant les informations de la demande de cosignature
     *                 - dateCreation : Date de creation de la demande de signature
     *                 - description : Description de la demande de signature
     *                 - status : Status global de la demande de signature
     *                 - fileInfos : Tableau contenant la liste des informations des fichiers à signer/signés
     *                     * idFile : Id du fichier
     *                     * fileName : Nom du fichier
     *                     * cosignersWithStatus : Status de la signature pour chacun des signataires 
     *                         + id : Id du signataire
     *                         + status : Status de la signature
     *                 - cosignerInfos : Tableau contenant la liste des informations du/des signataires
     *                     * firstName : Prénom
     *                     * lastName : Nom
     *                     * mail : Email
     *                     * proofLevel : Niveau de preuve de la signature
     *                     * isCosignerCalling : 
     *                     * id : Id du signataire
     *                     * phone : Numéro de téléphone du signataire
     *                 - initiator : Tableau contenant les informations de l'initiateur de la demande de signature
     *                     * name : Nom + Prénom
     *                     * email : Email
     */
    public function getInfoCosignFromToken($token) 
    {
        $params = array('token' => $token);

        $client = $this->setClientSoap($this->URL_WSDL_COSIGN);
        $result = $client->call('getInfosFromCosignatureDemand', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders(false));

        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring; 
            if(strstr(strtolower($client->faultstring), 'invalid token'))
            {
                return 'INVALID_TOKEN';
            }
            else
            {
                return false;
            }            
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                return $result;
            }
        }
    }
        
    /**
     * fonction permettant d'obtenir le listing des co-signatures
     *
     * @param  array $options
     *             - [ search: Chaine de caractères permettant une recherche ]
     *             - [ firstResult: index de début de recherche ]
     *             - [ count: Nombre de résultats voulu ]
     *             - [ status: Statut des signatures demandés ]
     *             - [ dateBegin: Date de début ]
     *             - [ dateEnd: Date de fin ]
     * @return array contenant pour chaque item
     *             - cosignatureEvent : Id de la demande de cosignature
     *             - dateCreation : Date de creation de la demande de signature
     *             - status : Status global de la demande de signature
     *             - fileInfos : Tableau contenant la liste des informations des fichiers à signer/signés
     *                 * idFile : Id du fichier
     *                 * fileName : Nom du fichier
     *                 * cosignersWithStatus : Status de la signature pour chacun des signataires 
     *                     + id : Id du signataire
     *                     + status : Status de la signature
     *             - cosignerInfos : Tableau contenant la liste des informations du/des signataires
     *                 * firstName : Prénom
     *                 * lastName : Nom
     *                 * mail : Email
     *             - initiator : Tableau contenant les informations de l'initiateur de la demande de signature
     *                 * name : Nom + Prénom
     *                 * email : Email
     */
	public function getListCosign(array $options = array())
	{
		// Paramètre du service de connexion
        // @todo supprimer ce paramètre ??
        $params = $options;
        $client = $this->setClientSoap($this->URL_WSDL_COSIGN);
		$result = $client->call('getListCosign', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());		                                               

		if ($client->fault) 
		{
            $this->errors[] = $client->faultstring; 
			return false;
		} 
		else 
		{
			$err = $client->getError();
			if ($err) 
			{
                $this->errors = $err; 
				return $err;
			} 
			else 
			{
                // Si aucun résultat, une chaine vide est retournée, on la transforme en tableau vide
                if(empty($result))
                    $result = array();
				
                // Si un seul résultat, on l'englobe dans un tableau
                if(isset($result['status']))
                    $result = array($result);
                
                return $result;
			}
		}
	}

    /**
     * fonction permettant d'obtenir le listing des signatures
     * @return Liste des signatures accompagnées de leurs propriétés
     */
    public function getListSign()
    {
        // Paramètre du service de connexion
        $params = array();
        $client = $this->setClientSoap($this->URL_WSDL_SIGN);
        $result = $client->call('getListSign', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());                                                     

        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring; 
            return false;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return $err;
            } 
            else 
            {
                return $result;
            }
        }
    }
        
    /**
     * Permet d'ajouter une nouvelle société cliente
     * 
     * @param type $name : nom de la société
     * @param type $siret : numéro de siret de la société
     * @param type $adress : adresse du siège de la société
     * @param type $zipCode : code postal du siège de la société
     * @param type $city : ville du siège de la société
     * @param type $phone : numéro de téléphone de la société
     * //@TODO : terminer ce service
     */
    public function addSociety($siret)
    {
        // Paramètre du service de connexion
        $params = array(
                        'siret' => $siret                            
                        );
        
        $client = $this->setClientSoap($this->URL_WSDL_REGISTRATION);
        $result = $client->call('addSociety', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders(false));

        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring; 		
            return false;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                return $result;                    
            }
        }
    }
    
    /**
     * Permet d'ajouter un nouveau utilisateur associé à une société
     * 
     * @param type $societyRef : référence de la société
     * @param type $lastname : nom de l'utilisateur
     * @param type $firstname : prénom de l'utilisateur
     * @param type $address : adresse de localisation de l'utilisateur
     * @param type $zipCode : code postal de l'utilisateur
     * @param type $city : ville de l'utilisateur
     * @param type $phone : numéro de téléphone de l'utilisateur
     * @param type $email : adresse mail de l'utilisateur
     * @param type $admin : TRUE si l'utilisateur est un admin, FALSE sinon
     * @param type $password : password en sha1
     * @param type $lang : langue de l'utilisateur (aujourd'hui fr ou en)
     */
    public function addUser($societyRef, $lastname, $firstname, $address, 
                            $zipCode, $city, $phone, $email, $admin, $password, $product, $lang, $audit)
    {
        // Paramètre du service de connexion
        $params = array(
                        'societyReference' => $societyRef,
                        'email' => $email,
                        'title' => '',
                        'lastname' => $lastname,
                        'firstname' => $firstname,
                        'phoneNumber' => $phone,
                        'address' => $address,
                        'zipCode' => $zipCode,
                        'city' => $city,
                        'authenticationType' => 'sms',
                        'audit' => $audit,
                        'admin' => $admin,
                        'password' => $password,
                        'productRef' => $product,
                        'language' => $lang
                        );
        
        $client = $this->setClientSoap($this->URL_WSDL_REGISTRATION);
        $result = $client->call('addUser', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders(false));         

        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring; 
            return false;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                if($result == false || $result == 'false')
                {
                    return false;
                }
                return $result;                    
            }
        }
    }

    
    /**
     * Permet de supprimer une co-signature
     * 
     * @param  integer $idDemand Id de la demande de cosignature à supprimer
     * @return boolean
     */
    public function deleteCosignDemand($idDemand)
    {
        // Paramètre du service de connexion
        $params = array('idDemand' => $idDemand);
        
        $client = $this->setClientSoap($this->URL_WSDL_COSIGN);
        $result = $client->call('cancelCosignatureDemand', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());         

        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring; 
            return false;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                if($result == false || $result == 'false')
                {
                    return false;
                }
                return $result;                    
            }
        }
    }

    /**
     * Permet de relancer les co-signataires n'ayant pas signé lors d'une demande de cosignature
     * 
     * @param  integer $idDemand : Id de la demande de cosignature
     * @return booléen
     */
    public function alertCosigners($idDemand)
    {
        // Paramètre du service de connexion
        $params = array(
                        'idDemand' => $idDemand                            
                        );
        
        $client = $this->setClientSoap($this->URL_WSDL_COSIGN);
        $result = $client->call('alertCosigners', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());         
                    
        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring;  
            return false;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                if($result == false || $result == 'false')
                {
                    return false;
                }
                return $result;                    
            }
        }
    }
    
    /**
     * Permet de demander une réinitialisation de mot de passe
     * 
     * @param type $mail : adresse mail de demande de réinitialisation
     */
    public function forgetPassword($mail)
    {
        // Paramètre du service de connexion
        $params = array(
                        'mail' => $mail
                        );
        
        $client = $this->setClientSoap($this->URL_WSDL_REGISTRATION);
        $result = $client->call('forgetPassword', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders(false));         
                    
        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring; 
            return false;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                if($result === true || strtolower(trim($result)) == 'true')
                {
                    return true;
                }
                return $result;                    
            }
        }
    }

    /**
     * Fonction permettant de connaitre le statut d'un token
     * @param token
     * @return statut
     */
    public function getTokenStatus($token) 
    {
        $params = array('tokenWithAction' => $token);

        $client = $this->setClientSoap($this->URL_WSDL_COMMON);
        $result = $client->call('getTokenStatus', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders(false));

        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring; 
            return $client->faultstring;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return $err;
            } 
            else 
            {
                return $result;
            }
        }
    }
    
    /**
    * Fonction permettant de valider l'adresse mail d'un utilisateur après inscription
    * @param token
    * @return statut
    */
    public function validMail($token, $otp = null) 
    {
        $params = array(
            'token' => $token,
            'otp' => $otp
        );

        $client = $this->setClientSoap($this->URL_WSDL_REGISTRATION);
        $result = $client->call('validMail', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders(false));            
        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring; 
            return false;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                if($result == null || empty($result))
                {
                    $this->errors[] = 'No result'; 
                    return false;
                }
                else
                {
                    return $result;
                }
            }
        }
    }
    
    /**
     * Cette fonction permet d'obtenir le statut d'un utilisateur.
     *
     *  USER_AUDITED_OK = 1;
     *  USER_NOT_AUDITED_OK = 2;
     *  USER_FROZEN = 3;
     *  USER_REVOKED = 4;
     *  USER_PENDING_VALIDATION = 5;
     * 
     * @return type
     */
    public function getUserStatus() 
    {
        $params = array();
        $client = $this->setClientSoap($this->URL_WSDL_AUDIT);
        $result = $client->call('getUserStatus', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());

        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring; 
            return false;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                return $result;
            }
        }
    }
    
    /**
     * Cette fonction permet d'obtenir le status d'un fichier d'audit.
     * -> Pièce d'identité ou kbis entreprise
     *
     * Status possible pour la carte d'identité
     *     [KBIS|ID_CARD]_AUDIT_OK              : Document validé
     *     [KBIS|ID_CARD]_AUDIT_NOT_OK          : Document rejeté (non utilisé) 
     *     [KBIS|ID_CARD]_AUDIT_NOT_REQUIRED    : Document pas obligatoire pour signé, mais identité non validé
     *     [KBIS|ID_CARD]_AUDIT_TO_CHECK        : Document reçu et en cours de vérification
     *     [KBIS|ID_CARD]_AUDIT_WAITING         : Document en attente d'être recu
     *     [KBIS|ID_CARD]_AUDIT_WAITING_2       : Document reçu et non validé, mais possibilité d'être renvoyé
     *
     * 
     * @param $fileType : type du fichier à traiter
     *                      * IDCARD 
     *                      * KBIS
     * @return type
     */
    public function getAuditFileStatus($fileType) 
    {
        $params = array(
            'fileType' => $fileType
        );
        $client = $this->setClientSoap($this->URL_WSDL_AUDIT);
        $result = $client->call('getAuditFileStatus', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());
        
        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring; 
            return false;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                return $result;
            }
        }
    }
    
    /**
     * Cette fonction permet d'envoyer un fichier à auditer
     * -> Pièce d'identité ou kbis entreprise
     * @param $fileType : type du fichier à traiter
     *                      * IDCARD 
     *                      * KBIS
     * @param $file : fichier en base64
     * @return type
     */
    public function sendAuditFile($fileType, $file) 
    {
        $params = array(
            'fileType' => $fileType,
            'fileContent' => $file
        );
        $client = $this->setClientSoap($this->URL_WSDL_AUDIT);
        $result = $client->call('sendAuditFile', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());

        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring; 
            return false;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                if(trim(strtolower($result)) == 'true' || $result === true)
                {
                    return true;
                }
                else
                {
                    $this->errors[] = 'No result'; 
                    return false;
                }
            }
        }
    }
    
    /**
     * Cette fonction permet d'obtenir le status d'un fichier d'audit.
     * -> Pièce d'identité ou kbis entreprise
     * @param $fileType : type du fichier à traiter
     *                      * IDCARD 
     *                      * KBIS
     * @return type
     */
    public function getPhoneNumberStatus() 
    {
        $params = array();
        $client = $this->setClientSoap($this->URL_WSDL_AUDIT);
        $result = $client->call('getPhoneNumberStatus', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());
        
        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring; 
            return false;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                return $result;
            }
        }
    }
    
    /**
     * Cette fonction permet de changer le produit d'un utilisateur (forfait)
     * @param $product : nouveau produit
     * 
     * @return type
     */
    public function setProduct($product)
    {
        $params = array(
            'productRef' => $product
        );
        $client = $this->setClientSoap($this->URL_WSDL_REGISTRATION);
        $result = $client->call('setProduct', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());

        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring; 
            return false;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                if($result === true || strtolower(trim($result)) == 'true')
                {
                    return true;
                }
                return false;
            }
        }
    }

    /**
     * Cette fonction permet de faire envoyer un sms afin de valider une action,
     * en fonction d'un token d'authnetification.
     * @param $token : le token doit être préfixé de l'action à valider ('subscribe' par ex).
     * 
     * @return type
     */
    public function sendOtp($token) 
    {
        $params = array('tokenWithAction' => $token);

        $client = $this->setClientSoap($this->URL_WSDL_REGISTRATION);
        $result = $client->call('sendOtp', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders(false));

        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring; 
            return $client->faultstring;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return $err;
            } 
            else 
            {
                if($result === true || strtolower(trim($result)) == 'true')
                {
                    return true;
                }
                
                $this->errors[] = 'No result'; 
                return false;
            }
        }
    }
    
    /**
     * Cette fonction permet de valider la réinitialiser ou modifier le mot de passe
     * @param $token        
     * @param type $oldPwd
     * @param type $newPwd1
     * @param type $newPwd2
     * @param type $otp
     *   
     * @return type
     */
    public function setUserPassword($token, $oldPass, $newPass1, $newPass2, $otp)
    {
        $params = array(
            'token' => $token,
            'oldPwd' => $oldPass,
            'newPwd1' => $newPass1,
            'newPwd2' => $newPass2,
            'otp' => $otp
        );
        $client = $this->setClientSoap($this->URL_WSDL_REGISTRATION);
        
        // Si l'otp et le token sont null, on appelle la fonction avec un headers contenant
        // les données d'authentification de l'utilisateur
        if($token == null || (empty($token)) && ($otp == null || empty($otp)))
        {
            $result = $client->call('setPassword', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());
        }
        else
        {
            $result = $client->call('setPassword', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders(false));
        }

        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring; 
            
            if(strstr(strtolower($client->faultstring), 'old password missmatch'))
            {
                return 'OLD_PASSWORD_MISSMATCH';
            }
            else
            {
                return false;
            } 
        } 
        else 
        {
            if($result === true || strtolower(trim($result)) == 'true')
            {
                return true;
            }
            return false;
        }
    }

    /**
     * Cette fonction permet de demander une validation du numéro de teléphone grâce à un code OTP
     * 
     * @param $token : le token de la demande.
     * 
     * @return type
     */
    public function validPhoneNumber($token, $otp) 
    {
        $params = array(
            'otp' => $otp,
            'token' => $token
        );
        $client = $this->setClientSoap($this->URL_WSDL_REGISTRATION);
        $result = $client->call('validPhoneNumber', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());
        
        if ($client->fault) 
        {
            $this->errors[] = $client->faultstring; 
            return $client->faultstring;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return $err;
            } 
            else 
            {
                if($result === true || strtolower(trim($result)) == 'true')
                {
                    return true;
                }
                else
                {
                    return $result;
                }
            }
        }
    }
       
    /**
     * Cette fonctione permet d'indiquer si l'adresse mail correspond à un utilisateur audité
     * @param $email : email à traiter
     * @return TRUE si l'email correspond à un utilisateur audité, FALSE sinon
     */
    public function isAuditedUser($email)
    {
        $params = array(
                'mail' => $email
            );
            $client = $this->setClientSoap($this->URL_WSDL_AUDIT);
            $result = $client->call('isAuditedUser', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders(false));            

            if ($client->fault) 
            {
                $this->errors[] = $client->faultstring; 
                return false;
            } 
            else 
            {
                $err = $client->getError();
                if ($err) 
                {
                    $this->errors = $err; 
                    return false;
                } 
                else 
                {
                    if($result === true || strtolower(trim($result)) == 'true')
                    {
                        return true;
                    }
                    else
                    {
                        $this->errors[] = 'No result'; 
                        return false;
                    }
                }
            }
    }
        
	/**
	 * Permet de retourner l'état d'authentification de l'utilisateur courant
	 * @return TRUE si l'utilisateur est authentifié, FALSE sinon
	 */
	public function isAuthenticated()
	{
	    return $this->isAuthenticated;
	}

    /***********************************************************
     * WEB SERVICES D'ARCHIVAGE
     ***********************************************************/	

    /**
     * Fonction permettant d'archiver un ensemble de documents
     * @param @TODO : commenter les params
     */
    public function archive($fileB64, $fileName, $subject, $date2, $type, $author, $comment, $ref, $amount, $tagsLst)
    {          
        $payload = "";
        
        $client = $this->setClientSoap($this->URL_WSDL_ARCHIVE);
        
        // Construction du tableau des paramètres
        $fileParam = array( 'content' => $fileB64,
                            'fileName' => $fileName,
                            'subject' => $subject,
                            'date1' => date(DATE_RFC2822),
                            'date2' => $date2,
                            'type' => $type,
                            'author' => $author,
                            'comment' => $comment,
                            'ref' => $ref,
                            'amount' => $amount,
                            'generic1' => 'gen1',
                            'generic2' => 'gen2',
                            'tag' => $tagsLst);

        $params = array(
                'file' => $fileParam,
            );
        
        $result = $client->call('archive', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());            
        

        if ($client->fault)
        {
            $this->errors[] = $client->faultstring; 
            return false;
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                return $result;
            }
        }
    }

    /**
     * Fonction permettant de récupérer une archive
     * @param idDemand : id de la demande à confirmer
     * @param idFile : id du fichier signé à récupérer
     * @ TODO : récupérer tous les fichiers d'un coup (dans un zip)
     * @ TODO : cette fonction doit remplacer getSignedFilesFromDemand()
     */
    public function getArchivedFile($iua)
    {            
        // Paramètre du service de connexion
        $params = array(
                        'iua' => $iua
                        );

        $client = $this->setClientSoap($this->URL_WSDL_ARCHIVE);
        
        $result = $client->call('getArchive', $params, self::API_NAMESPACE, self::API_NAMESPACE, $this->createHeaders());

        if ($client->fault) 
        {   
            $this->errors[] = $client->faultstring; 
            
            // La demande n'est pas associée à l'utilisateur
            if(strstr($client->faultstring, "is not associated with the user"))
            {                    
                return "DEMAND_NOT_ALLOWED";
            }
            else
            {
                // La demande n'a pas été validée, il est impossible de récupérer les fichiers signés
                if(strstr($client->faultstring, "has not been signed. Impossible to get signed files"))
                {
                    return "DEMAND_NOT_CONFIRMED";
                }
                else
                {
                    // Une erreur inconnue est apparue
                    return false;
                }
            }
        } 
        else 
        {
            $err = $client->getError();
            if ($err) 
            {
                $this->errors = $err; 
                return false;
            } 
            else 
            {
                // Tableau contenant :
                //      - fileName : le nom du fichier
                //      - file : le fichier signé
                return $result;
            }
        }
    }

    /***********************************************************
     * FIN WEB SERVICES D'ARCHIVAGE
     ***********************************************************/
}

